# nodehun-server

A webserver for the [Nodehun package](https://github.com/Wulf/nodehun/)
using the [Koa framework](https://koajs.com/). Developed for [ceskeslovniky.cz](https://ceskeslovniky.cz)
to provide access to Czech Hunspell dictionaries.

Requirements: Node.js >= 10

Installation: `npm install` and run the script: `node nodehun_server.js`.

## API

`GET /check?words=WORDS`

* `WORDS`: words to be checked separated by space

Returns a list of object consisting of properties:
* `word`: the original word,
* `correct`: whether the word was found in the dictionary,
* `suggestions`: list of suggested correct words for the checked word.