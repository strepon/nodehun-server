const nodehun = require("nodehun");
const koa = require("koa");
const router = require("koa-router")();
const cors = require("@koa/cors");
const fs = require("fs");
const util = require("util");

// promisify not needed since Node 11
fs.readFileAsync = util.promisify(fs.readFile);
const getFile = function(fileName) {
    return fs.readFileAsync(fileName);
};

const getDictFiles = function() {
    const promises = [];
    promises.push(getFile("cs_CZ.aff"));
    promises.push(getFile("cs_CZ.dic"));

    return Promise.all(promises);
};

const runServer = async function() {
    const dictFiles = await getDictFiles();
    const dict = new nodehun(dictFiles[0], dictFiles[1]);

    const app = new koa();
    const portNumber = process.argv[2] || 8080;
    app.listen(portNumber);

    const getSuggestions = function(words) {
        const obtainedSuggestions = [];
        for (let w = 0; w < words.length; w++) {
            obtainedSuggestions.push(dict.suggest(words[w]));
        }
        return Promise.all(obtainedSuggestions);
    };

    router.get("/check", async(ctx, next) => {
        const url = new URL("http://localhost:8080/" + ctx.url);
        let words = url.searchParams.get("words");
        words = words.split(" ");

        try {
            const suggestions = await getSuggestions(words);
            const checkedWords = [];
            for (let w = 0; w < suggestions.length; w++) {
                checkedWords.push({
                    word: words[w], correct: suggestions[w] == null, suggestions: suggestions[w] == null ? [] : [suggestions[w]]
                });
            }
            ctx.body = checkedWords;
        }
        catch (error) {
            console.error(error);
        }
        await next();
    });

    function checkOrigin(ctx) {
        const requestOrigin = ctx.accept.headers.origin;
        if (["https://ceskeslovniky.cz", "https://www.ceskeslovniky.cz", "http://localhost:4000"].includes(requestOrigin)) {
            return requestOrigin;
        }
        else {
            return ctx.throw("Not valid origin.");
        }
    }

    app.use(cors({ origin: checkOrigin }));
    app.use(router.routes());
};

runServer();
